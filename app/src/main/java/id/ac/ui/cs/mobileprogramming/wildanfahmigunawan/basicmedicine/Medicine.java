package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.UUID;


@Entity(tableName = "medicine_table")
public class Medicine {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "supply")
    private int supply;

    @ColumnInfo(name = "hour")
    private int hour;

    @ColumnInfo(name = "minute")
    private int minute;

    @Ignore
    private Context context;


    public Medicine(){

    }
    public Medicine(Context context){
        this.name="";
        this.supply=0;
        this.hour=0;
        this.minute=0;
    }


    public Medicine(String nama, int supply, int hour, int minute){
        this.name=nama;
        this.supply=supply;
        this.hour=hour;
        this.minute=minute;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupply() {
        return supply;
    }

    public void setSupply(int supply) {
        this.supply = supply;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void toIntent (Intent intent){
        intent.putExtra("medicine_name", name);
        intent.putExtra("medicine_supply", supply);
        intent.putExtra("medicine_hour", hour);
        intent.putExtra("medicine_minute", minute);
    }

    public void fromIntent (Intent intent){
        name = intent.getStringExtra("medicine_name");
        supply = intent.getIntExtra("medicine_supply",0);
        hour = intent.getIntExtra("medicine_hour",0);
        minute = intent.getIntExtra("medicine_minute",0);
    }
}
