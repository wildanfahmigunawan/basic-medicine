package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class About extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.about);
    }
}