package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class MedicineViewModel extends AndroidViewModel {

    private MedicineRepository medicineRepository;
    private LiveData<Medicine[]> allMedicine;

    public MedicineViewModel (Application application){
        super(application);
        medicineRepository = new MedicineRepository(application);
        allMedicine = medicineRepository.getAllMedicine();
    }

    LiveData<Medicine[]> getAllMedicine() {
        return allMedicine;
    }

    public void insert (Medicine medicine){
        medicineRepository.insert(medicine);
    }
    public void update (Medicine medicine) { medicineRepository.update(medicine);}
    public Medicine getMedicinebyId (int id) { return medicineRepository.getMedicinebyId(id);}
    public void delete (Medicine medicine) { medicineRepository.delete(medicine);}
}

