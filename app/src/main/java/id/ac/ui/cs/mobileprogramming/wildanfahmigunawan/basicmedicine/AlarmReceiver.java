package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        Intent newIntent = new Intent(context, AlarmNotification.class);
        Medicine medicine = new Medicine(context);
        medicine.fromIntent(intent);
        medicine.toIntent(newIntent);

        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        context.startActivity(newIntent);
    }
}
