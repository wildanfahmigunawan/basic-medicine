package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {Medicine.class}, version = 2, exportSchema = false)
public abstract class BasicMedicineRoomDatabase extends RoomDatabase {

    public abstract MedicineDao medicineDao();

    private static volatile BasicMedicineRoomDatabase INSTANCE;

    static BasicMedicineRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (BasicMedicineRoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            BasicMedicineRoomDatabase.class, "basicmedicine_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback).build();
                }
            }
        }
        return INSTANCE;
    }

    public static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDBAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDBAsync extends AsyncTask<Void,Void,Void> {
        private final MedicineDao mDao;

        PopulateDBAsync(BasicMedicineRoomDatabase db){
            mDao = db.medicineDao();
        }

        @Override
        protected Void doInBackground(final Void... params){
//            mDao.deleteAll();
//            Medicine medicine = new Medicine("Paracetamol",10,12,15);
//            mDao.insert(medicine);
//            medicine = new Medicine("Amocillin",20,13,12);
//            mDao.insert(medicine);
            return null;
        }
    }
}
