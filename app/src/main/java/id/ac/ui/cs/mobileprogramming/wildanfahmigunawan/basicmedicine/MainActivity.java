package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private MedicineViewModel medicineViewModel;
    private Medicine mCurrentMedicine;
    private MedicineListAdapter medicineListAdapter;

    public static final int NEW_MEDICINE_ACTIVITY_REQUEST_CODE = 0;
    public static final int EDIT_MEDICINE_ACTIVITY_REQUEST_CODE = 1;

    private final int CONTEXT_MENU_EDIT = 0;
    private final int CONTEXT_MENU_DELETE = 1;
    private final int CONTEXT_MENU_DUPLICATE = 2;

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            new getByIdforListener(position).execute();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final MedicineListAdapter adapter = new MedicineListAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setItemClickListener(onItemClickListener);
        registerForContextMenu(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        medicineViewModel = new ViewModelProvider(this).get(MedicineViewModel.class);

        medicineViewModel.getAllMedicine().observe(this, new Observer<Medicine[]>() {
            @Override
            public void onChanged(Medicine[] medicines) {
                adapter.setMedicines(medicines);
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewMedicineActivity.class);
                startActivityForResult(intent, NEW_MEDICINE_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_about) {
            Intent intent = new Intent(getApplicationContext(), About.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_MEDICINE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Medicine medicine =
                    new Medicine(extras.getString("medicine_name"),extras.getInt("medicine_supply"),
                            extras.getInt("medicine_hour"),extras.getInt("medicine_minute"));
            medicineViewModel.insert(medicine);
        } else if (requestCode == EDIT_MEDICINE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Medicine medicine =
                    new Medicine(extras.getString("medicine_name"),extras.getInt("medicine_supply"),
                            extras.getInt("medicine_hour"),extras.getInt("medicine_minute"));
            medicineViewModel.update(medicine);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        if (v.getId() == R.id.recyclerview)
        {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

            new getByIdforMenu(info.position, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int index = item.getItemId();

        if (index == CONTEXT_MENU_EDIT)
        {
            new getByIdforMenuEdit(info.position);
        }
        else if (index == CONTEXT_MENU_DELETE)
        {
            new getByIdforMenuDelete(info.position);
        }
        else if (index == CONTEXT_MENU_DUPLICATE)
        {
            new getByIdforMenuDuplicate(info.position);
        }

        return true;
    }

    public class getByIdforMenu  extends AsyncTask<Void,Void,Medicine>{

        private int id;
        private ContextMenu menu;

        getByIdforMenu(int id, ContextMenu contextMenu){
            this.id = id;
            this.menu = contextMenu;
        }

        @Override
        protected Medicine doInBackground(Void... voids) {
            return medicineViewModel.getMedicinebyId(id);
        }

        @Override
        protected void onPostExecute(Medicine medicine) {
            super.onPostExecute(medicine);
            menu.setHeaderTitle(medicine.getName());
            menu.add(Menu.NONE, CONTEXT_MENU_EDIT, Menu.NONE, "Edit");
            menu.add(Menu.NONE, CONTEXT_MENU_DELETE, Menu.NONE, "Delete");
            menu.add(Menu.NONE, CONTEXT_MENU_DUPLICATE, Menu.NONE, "Duplicate");
        }
    }

    public class getByIdforListener  extends AsyncTask<Void,Void,Medicine>{

        private int id;

        getByIdforListener(int id){
            this.id = id;
        }

        @Override
        protected Medicine doInBackground(Void... voids) {
            Log.i("posisi",Integer.toString(id));
            return medicineViewModel.getMedicinebyId(id);
        }

        @Override
        protected void onPostExecute(Medicine medicine) {
            super.onPostExecute(medicine);
            Intent intent = new Intent(getApplicationContext(), NewMedicineActivity.class);
            mCurrentMedicine = medicine;
            mCurrentMedicine.toIntent(intent);
            MainActivity.this.startActivityForResult(intent, EDIT_MEDICINE_ACTIVITY_REQUEST_CODE);
        }
    }

    public class getByIdforMenuEdit  extends AsyncTask<Void,Void,Medicine>{

        private int id;

        getByIdforMenuEdit(int id){
            this.id = id;
        }

        @Override
        protected Medicine doInBackground(Void... voids) {
            return medicineViewModel.getMedicinebyId(id);
        }

        @Override
        protected void onPostExecute(Medicine medicine) {
            super.onPostExecute(medicine);
            Intent intent = new Intent(getBaseContext(), NewMedicineActivity.class);
            mCurrentMedicine=medicine;
            mCurrentMedicine.toIntent(intent);
            startActivityForResult(intent, EDIT_MEDICINE_ACTIVITY_REQUEST_CODE);
        }
    }

    public class getByIdforMenuDelete  extends AsyncTask<Void,Void,Medicine>{

        private int id;

        getByIdforMenuDelete(int id){
            this.id = id;
        }

        @Override
        protected Medicine doInBackground(Void... voids) {
            return medicineViewModel.getMedicinebyId(id);
        }

        @Override
        protected void onPostExecute(Medicine medicine) {
            super.onPostExecute(medicine);
            medicineViewModel.delete(medicine);
        }
    }

    public class getByIdforMenuDuplicate  extends AsyncTask<Void,Void,Medicine>{

        private int id;

        getByIdforMenuDuplicate(int id){
            this.id = id;
        }

        @Override
        protected Medicine doInBackground(Void... voids) {
            return medicineViewModel.getMedicinebyId(id);
        }

        @Override
        protected void onPostExecute(Medicine medicine) {
            super.onPostExecute(medicine);
            Medicine newMedicine = new Medicine(getApplicationContext());
            Intent intent = new Intent();

            medicine.toIntent(intent);
            newMedicine.fromIntent(intent);
            newMedicine.setName(medicine.getName() + " (copy)");
            medicineViewModel.insert(newMedicine);
        }
    }

}
