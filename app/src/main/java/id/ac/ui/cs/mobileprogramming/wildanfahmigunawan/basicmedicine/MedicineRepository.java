package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

public class MedicineRepository {

    private static class insertAsyncTask extends AsyncTask<Medicine, Void, Void>{

        private MedicineDao mAsyncTaskDao;

        insertAsyncTask(MedicineDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Medicine... params){
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Medicine, Void, Void>{
        private MedicineDao mAsyncTaskDao;

        updateAsyncTask(MedicineDao dao) { mAsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(final Medicine... params){
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class getMedicinebyIdAsyncTask extends AsyncTask<Integer, Void, Medicine>{
        private MedicineDao mAsyncTaskDao;

        getMedicinebyIdAsyncTask(MedicineDao dao) { mAsyncTaskDao = dao; }

        @Override
        protected Medicine doInBackground(final Integer... params){
            Medicine medicine = mAsyncTaskDao.getMedicineById(params[0]);
            return medicine;
        }

        @Override
        protected void onPostExecute(Medicine medicine){
            super.onPostExecute(medicine);

        }
    }

    private MedicineDao mMedicineDao;
    private LiveData<Medicine[]> mAllMedicine;

    MedicineRepository(Application application){
        BasicMedicineRoomDatabase db = BasicMedicineRoomDatabase.getDatabase(application);
        mMedicineDao = db.medicineDao();
        mAllMedicine = mMedicineDao.getAllMedicine();
    }

    private Medicine handleMedicine (Medicine medicine){
        return  medicine;
    }

    LiveData<Medicine[]> getAllMedicine(){
        return mAllMedicine;
    }

    public void insert (Medicine medicine){
        new insertAsyncTask(mMedicineDao).execute(medicine);
    }
    public void update (Medicine medicine) { new updateAsyncTask(mMedicineDao).execute(medicine);}
    public Medicine getMedicinebyId (Integer id) {return mMedicineDao.getMedicineById(id);}
    public void delete (Medicine medicine) { mMedicineDao.delete(medicine);}
}
