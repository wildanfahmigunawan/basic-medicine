package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MedicineListAdapter extends RecyclerView.Adapter<MedicineListAdapter.MedicineViewHolder>{

    private Context mContext;
    private AlarmManager mAlarmManager;
    private View.OnClickListener onItemClickListener;

    public void setItemClickListener(View.OnClickListener clickListener) {
        onItemClickListener = clickListener;
    }

    class MedicineViewHolder extends RecyclerView.ViewHolder{
        private final TextView medicinenameItemView;
        private final TextView medicinesupplyItemView;
        private final TextView medicinealarmItemView;


        private MedicineViewHolder(View itemView){
            super(itemView);
            medicinenameItemView = itemView.findViewById(R.id.medicinenametextView);
            medicinesupplyItemView = itemView.findViewById(R.id.medicinesupplytextView);
            medicinealarmItemView = itemView.findViewById(R.id.medicinealarmtextView);
            itemView.setTag(this);
            itemView.setOnClickListener(onItemClickListener);
        }
    }

    private final LayoutInflater mInflater;
    private Medicine[] Medicines;

    MedicineListAdapter(Context context ){
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mAlarmManager = (AlarmManager)context.getSystemService(mContext.ALARM_SERVICE);

    }

    @Override
    public MedicineViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new MedicineViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MedicineViewHolder holder, int position){
        if (Medicines != null){
            Medicine current = Medicines[position];
            holder.medicinenameItemView.setText(current.getName());
            holder.medicinesupplyItemView.setText(Integer.toString(current.getSupply()) + " Left" );
            holder.medicinealarmItemView.setText("Next Alarm "+Integer.toString(current.getHour())+":"+Integer.toString(current.getMinute()));
        }
    }

    void setMedicines(Medicine[] medicines){
        Medicines = medicines;
        for (Medicine medicine : medicines){
            setAlarm(medicine);
        }
        notifyDataSetChanged();
    }

    private void setAlarm(Medicine medicine){
        PendingIntent sender;
        Intent intent;

        intent = new Intent(mContext, AlarmReceiver.class);
        medicine.toIntent(intent);
        sender = PendingIntent.getBroadcast(mContext, medicine.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, medicine.getHour());
        calendar.set(Calendar.MINUTE, medicine.getMinute());

        if (Calendar.getInstance().after(calendar)) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        
        mAlarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY, sender);
    }

    private void cancelAlarm(Medicine medicine)
    {
        PendingIntent sender;
        Intent intent;

        intent = new Intent(mContext, AlarmReceiver.class);
        sender = PendingIntent.getBroadcast(mContext, medicine.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.cancel(sender);
    }


    @Override
    public int getItemCount(){
        if (Medicines != null)
            return Medicines.length;
        else return 0;
    }
}