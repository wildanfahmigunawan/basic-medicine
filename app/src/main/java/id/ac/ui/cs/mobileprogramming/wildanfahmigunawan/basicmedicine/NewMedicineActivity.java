package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

public class NewMedicineActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY =
            "id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.mymedicine.REPLY";
    private EditText editMedicineView;
    private EditText editSupplyView;
    private EditText editAlarmView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_medicine);
        editMedicineView = findViewById(R.id.edit_medicine);
        editSupplyView = findViewById(R.id.edit_supply);
        editAlarmView = findViewById(R.id.choose_time);

        editAlarmView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(NewMedicineActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        editAlarmView.setText(String.format("%02d:%02d", hourOfDay, minutes));
                    }
                },0,0,true);
                timePickerDialog.show();
            }
        });


        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(editMedicineView.getText())){
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String medicine_name = editMedicineView.getText().toString();
                    String medicine_supply = editSupplyView.getText().toString();
                    String medicine_hour = editAlarmView.getText().toString().split(":")[0];
                    String medicine_minute = editAlarmView.getText().toString().split(":")[1];
                    replyIntent.putExtra("medicine_name", medicine_name);
                    replyIntent.putExtra("medicine_supply", Integer.parseInt(medicine_supply));
                    replyIntent.putExtra("medicine_hour", Integer.parseInt(medicine_hour));
                    replyIntent.putExtra("medicine_minute", Integer.parseInt(medicine_minute));
                    setResult(RESULT_OK,replyIntent);
                }
                finish();
            }
        });
    }
}
