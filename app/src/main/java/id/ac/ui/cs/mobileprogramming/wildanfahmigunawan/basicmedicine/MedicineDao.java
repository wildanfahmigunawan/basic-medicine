package id.ac.ui.cs.mobileprogramming.wildanfahmigunawan.basicmedicine;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MedicineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Medicine medicine);

    @Update
    void update(Medicine medicine);

    @Delete
    void delete(Medicine medicine);

    @Query("DELETE FROM medicine_table")
    void deleteAll();

    @Query("SELECT * from medicine_table")
    LiveData<Medicine[]> getAllMedicine();

    @Query("SELECT name FROM medicine_table")
    List<String> getAllMedicineName();

    @Query("SELECT * from medicine_table WHERE name = :name")
    LiveData<Medicine> getMedicine(String name);

    @Query("SELECT * from medicine_table where id =:id")
    Medicine getMedicineById(int id);

}
